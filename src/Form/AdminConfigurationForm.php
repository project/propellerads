<?php

namespace Drupal\propellerads\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configuration form definition.
 */
class AdminConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['propellerads.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'propellerads_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('propellerads.settings');

    $link = Link::fromTextAndUrl('PropellerAds', Url::fromUri('http://propellerads.com'))->toString();    
    $form['verification_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site verification code'),
      '#description' => $this->t('Create a new site at @link and add here the verification code (only the attribute "content" of the <strong>meta</strong> tag).', ['@link' => $link]),
      '#default_value' => $config->get('verification_code'),
      '#attributes' => ['tabindex' => 1],
    ];

    $verification_code_enabled = $config->get('verification_code_enabled') === NULL ? 1 : $config->get('verification_code_enabled');
    $form['verification_code_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Site verification'),
      '#description' => $this->t('Normally you only need verification of the site at first time. When site is verified you can uncheck this to disable the <strong>meta</strong> used for site verification.'),
      '#default_value' => $verification_code_enabled,
      '#attributes' => ['tabindex' => 2],
    ];

    $form['channel_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel ID'),
      '#description' => $this->t('Create a new site at @link <strong>Dashboard > Menu > Sites</strong> and fill the Channel ID here.', ['@link' => $link]),
      '#default_value' => $config->get('channel_id'),
      '#attributes' => ['tabindex' => 3],
    ];

    $disable_admin_routes = $config->get('disable_admin_routes') === NULL ? 1 : $config->get('disable_admin_routes');
    $form['disable_admin_routes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable on admin routes.'),
      '#description' => $this->t('Normally you don\'t want to have PropellerAds activated on admin area.'),
      '#default_value' => $disable_admin_routes,
      '#attributes' => ['tabindex' => 4],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('propellerads.settings')
      ->set('verification_code', $form_state->getValue('verification_code'))
      ->set('verification_code_enabled', $form_state->getValue('verificaion_code_enabled'))
      ->set('channel_id', $form_state->getValue('channel_id'))
      ->set('disable_admin_routes', $form_state->getValue('disable_admin_routes'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}

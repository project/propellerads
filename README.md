### Introduction

This module serves for integrate Propeller Ads popups in your Drupal only activating and configuring this module.

### Setup procedure

Copy under modules folder. Go to modules page check it and enable it.

Later you should go to configuration page (or click configure on module page) and introduce your Publisher and Channel ID.

### Author

Daniel Gil Jara <danielgiljara@gmail.com>
